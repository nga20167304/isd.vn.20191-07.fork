package businessLayer.card;

import java.sql.SQLException;

import dataLayer.StationDAO;
import dataTransferObject.Card;

/**
 * @author Vu Duc Nguyen
 * email: ducnguyen272727@gmail.com
 *
 */
/**
 * @author Admin
 *
 */
public class CardBUS implements CardInterface {
	Card card = new Card();
	
	/**
	 *@param id
	 *@return
	 */
	public double getCardBalance(String id) {
		// xu ly cho moi loai card
		return card.getBalance();
	}
	
	/**
	 *@param id
	 *@return
	 */
	public boolean checkBalanceFirst(String id) {
		if (card.getBalance() > 1.9) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 *@param id
	 *@return
	 */
	public boolean checkBalanceWhenExit (String id) {
		if (card.getBalance() > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 *@param idStation1,idStation2
	 *@return
	 *@throws ClassNotFoundException, SQLException
	 */
	public double calculateFee(int idStation1, int idStation2) throws ClassNotFoundException, SQLException {
		double distance = Math.abs(StationDAO.getDistanceById(idStation1) - StationDAO.getDistanceById(idStation2));
		if( distance <= 5 ) {
			return 1.9;
		}else {
			return 1.9 + Math.ceil((distance-5)/2)*0.4;
		}
	}
	
	
	/**
	 *@param card
	 */
	public void displayCardInfor (Card card) {
		System.out.println("Type: Card \t ID: " + card.getId());
		System.out.println("Balance: "+ card.getBalance());
	}
}
