package businessLayer.card;

import java.sql.SQLException;
import java.text.DecimalFormat;

import businessLayer.TripBUS;
import dataLayer.PrepaidCardDAO;
import dataTransferObject.PrepaidCard;
import dataTransferObject.Trip;
import hust.soict.se.customexception.InvalidIDException;
import presentationLayer.CardScannerInterface;
import presentationLayer.GateInterface;

/** * @author Vu Duc Nguyen
 * email: ducnguyen272727@gmail.com
 *
 */
public class PrepaidCardBUS extends CardBUS implements CardInterface, PrepaidCardInterface{
	private static PrepaidCardBUS instance;
//	public PrepaidCardBUS(){}
	
	public static PrepaidCardBUS getInstance(){
		if(instance == null){
            instance = new PrepaidCardBUS();
        }
        return instance;
	}
	
	static PrepaidCardDAO prepaidCardDAO = PrepaidCardDAO.getInstance();
	static TripBUS tripBUS = TripBUS.getInstance();
	
	/**
	 * get Card Information by id
	 * @param id
	 * @return PrepaidCard
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public PrepaidCard getCardById( String id) throws ClassNotFoundException, SQLException {
		return prepaidCardDAO.getCardById(id);
	}
	
	/**
	 * get Card Information by code
	 * @param code
	 * @return PrepaidCard
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public PrepaidCard getCardByCode( String code) throws ClassNotFoundException, SQLException {
		return prepaidCardDAO.getCardByCode(code);
	}
	
	@Override
	public double getCardBalance(String id) {
		PrepaidCard card = null;
		try {
			card = prepaidCardDAO.getCardById(id);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return card.getBalance();
	}
	
	@Override
	public boolean checkBalanceFirst(String id) {
		double balance = getCardBalance(id);
		
		if (balance > 1.9) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	public boolean checkBalanceWhenExit (String id) {
		double balance = getCardBalance(id);
		
		if (balance > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 *@param id,calculateFee
	 *@return
	 */
	public double updateBalance(String id, double calculateFee) {
		double balance = getCardBalance(id);
		double updateBalance = balance - calculateFee;
		if(updateBalance < 0) {
			System.out.println("Error Balance < 0");
			return updateBalance;
		} else {
			try {
				prepaidCardDAO.updateBalance(id, updateBalance);
			} catch (ClassNotFoundException | SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return updateBalance;
		}
	}
	
	public void displayCardInfor (PrepaidCard card) {
		DecimalFormat df = new DecimalFormat("#.00");
		System.out.println("Type: Prepaid Card \t ID: " + card.getId());
		System.out.println("Balance: "+ df.format(card.getBalance()));
	}
	
	/**
	 * handle Card when enter station
	 * @param pseudoBarCode,stationID
	 *@throws InvalidIDException, ClassNotFoundException, SQLException
	 */
	public void handleCardWhenEnterStation(String pseudoBarCode, int stationID) throws  ClassNotFoundException, SQLException, InvalidIDException {
		String cardCode = CardScannerInterface.process(pseudoBarCode);
		PrepaidCard card = new PrepaidCard();
		card = getCardByCode(cardCode);
		
		if( card.getId() == null) {
			System.out.println("Card does not exist");
		} else if( tripBUS.getTripByTicketCardID(card.getId()).size() > 0) {
			System.out.println("Card is using");
			displayCardInfor(card);
		}else if( checkBalanceFirst(card.getId())) {
			GateInterface.open();
			displayCardInfor(card);
			tripBUS.createNewTrip(card.getId(), stationID);
		}else {
			System.out.println("Invalid Prepaid Card");
			displayCardInfor(card);
			System.out.println("Not enough balance: Expected 1.9 euros");
		}
	}
	
	/**
	 * handle Card when exit station
	 * @param pseudoBarCode,stationID
	 *@throws InvalidIDException, ClassNotFoundException, SQLException
	 */
	public void handleCardWhenExitStation(String pseudoBarCode, int stationID) throws InvalidIDException, ClassNotFoundException, SQLException {
		String cardCode = CardScannerInterface.process(pseudoBarCode);
		PrepaidCard card = new PrepaidCard();
		card = getCardByCode(cardCode);
		
		if( card.getId() == null) {
			System.out.println("Card does not exist");
			GateInterface.close();
		} else if( tripBUS.getTripByTicketCardID(card.getId()).size() != 1) {
			System.out.println("Card not used when entering the station");
		}else {
			Trip trip = tripBUS.getTripByTicketCardID(card.getId()).get(0);
			double fee = calculateFee(trip.getDepartureStationID(), stationID);
			
			if( fee > card.getBalance()) {
				System.out.println("Balance < fee, Please recharge your card");
			}else {
				card.setBalance(updateBalance(card.getId(), fee));
				tripBUS.updateTrip(trip.getId(), stationID, fee);
				GateInterface.open();
				displayCardInfor(card);
			}
		}
	}
}
