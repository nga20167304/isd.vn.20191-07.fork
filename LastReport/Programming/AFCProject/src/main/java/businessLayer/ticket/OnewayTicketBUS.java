package businessLayer.ticket;

import java.sql.SQLException;
import java.text.DecimalFormat;

import businessLayer.TripBUS;
import dataLayer.OnewayTicketDAO;
import dataLayer.StationDAO;
import dataTransferObject.OnewayTicket;
import dataTransferObject.Trip;
import hust.soict.se.customexception.InvalidIDException;
import presentationLayer.GateInterface;
import presentationLayer.TicketRecognizerInterface;

/**
 * @author Do Thuy Nga
 *
 */
public class OnewayTicketBUS extends TicketBUS implements OnewayTicketInterface{
	
	/**
	 * @param id
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static OnewayTicket getTicketById(String id) throws ClassNotFoundException, SQLException {
		return OnewayTicketDAO.findbyId(id);
	}
	
	/**
	 * @param code
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static OnewayTicket getTicketByCode(String code) throws ClassNotFoundException, SQLException {
		return OnewayTicketDAO.getByCode(code);
	}
	
	/**
	 * @param StationID
	 * @param owTicket
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static int checkEmbarkation(int StationID,OnewayTicket owTicket) throws ClassNotFoundException, SQLException {
		Double cmp=StationDAO.getDistanceById(StationID);
		Double enter=StationDAO.getDistanceById(owTicket.getEmbarkation());
		Double exit=StationDAO.getDistanceById(owTicket.getDisembarkation());
		if(cmp>=0 && cmp>=enter && cmp<=exit) return 1;
		return 0;

	}
	
	/**
	 * @param stationID
	 * @param owTicket
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static int checkDisembarkation(int stationID,OnewayTicket owTicket) throws ClassNotFoundException, SQLException {
		Double cmp=StationDAO.getDistanceById(stationID);
		int x=checkEmbarkation(stationID,owTicket);
		int enterStationID;
		enterStationID=owTicket.getEmbarkation();
		if(enterStationID!=0) {
			double price=caculate(enterStationID,stationID,owTicket);
			double distance=StationDAO.getDistanceById(enterStationID);
			if(distance < cmp && price >=0 && x==1) return  1;
		}
		return 0;
	}
	
	/**
	 * @param enterStationID
	 * @param exitStationID
	 * @param owTicket
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static double caculate(int enterStationID,int exitStationID,OnewayTicket owTicket)
			throws ClassNotFoundException, SQLException {
		Double enter=StationDAO.getDistanceById(enterStationID);
		Double exit=StationDAO.getDistanceById(exitStationID);
		Double distance=exit-enter;
		Double price=owTicket.getPrice();
		Double fare=(double) 0;
		if(distance<=5) 
			fare=1.9;
		else {
			fare=1.9;
			Double distance1=distance-5;
			while(distance1-2>=0) {
				fare+=0.4;
				distance1=distance1-2;
			}
		}
		owTicket.setPrice(price-fare);
		return owTicket.getPrice();
	}
	//Use Strategy for caculate Ticket
	/**
	 * @param owTicket
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void displayInfo(OnewayTicket owTicket) throws ClassNotFoundException, SQLException {
		DecimalFormat df = new DecimalFormat("#.00");
		System.out.println("Type: ONE-WAY TICKET\t"+"ID: "+owTicket.getId());
		System.out.println("Balance: "+ df.format(owTicket.getPrice())+" euros");
		
	}
	
	/**
	 * @param pseudoBarCode
	 * @param stationID
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void handleOnewayTicketWhenEnterStation(String pseudoBarCode, int stationID) throws InvalidIDException, ClassNotFoundException, SQLException {
		String ticketCode=TicketRecognizerInterface.process(pseudoBarCode);
		//Onewayticket
		OnewayTicket owTicket = new OnewayTicket();
		owTicket = getTicketByCode(ticketCode);
		if( owTicket.getId() == null) {
			System.out.println("Oneway Ticket does not exist");
			GateInterface.close();
		}else if(owTicket.getStatus() == 0) {
			if( checkEmbarkation(stationID,owTicket) ==1) {
				GateInterface.open();
				OnewayTicketBUS.displayInfo(owTicket);
				TripBUS.createNewTrip(owTicket.getId(), stationID);
				OnewayTicketDAO.updateStatus(owTicket.getId(), 1);
			} else {
				System.out.println("Invalid Station");
				OnewayTicketBUS.displayInfo(owTicket);
			}
		} else if (owTicket.getStatus() == 1) {
			System.out.println("Oneway Ticket is In Station");
			OnewayTicketBUS.displayInfo(owTicket);
		} else if (owTicket.getStatus() == 2) {
			System.out.println("Oneway Ticket is Destroyed");
			OnewayTicketBUS.displayInfo(owTicket);
		}
	}

	/**
	 * @param pseudoBarCode
	 * @param stationID
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void handleOnewayTicketWhenExitStation(String pseudoBarCode, int stationID) throws InvalidIDException, ClassNotFoundException, SQLException {
		// TODO Auto-generated method stub
		String ticketCode=TicketRecognizerInterface.process(pseudoBarCode);
		
		//Onewayticket
		OnewayTicket owTicket = new OnewayTicket();
		owTicket = OnewayTicketBUS.getTicketByCode(ticketCode);
		if( owTicket.getId() == null) {
			System.out.println("Oneway Ticket does not exist");
			GateInterface.close();
		}else if (owTicket.getStatus() == 1 ) {
			if( OnewayTicketBUS.checkDisembarkation(stationID,owTicket) ==1) {
				Trip trip = TripBUS.getTripByTicketCardID(owTicket.getId()).get(0);
				GateInterface.open();
				OnewayTicketBUS.displayInfo(owTicket);
				double price=caculate(trip.getDepartureStationID(), stationID, owTicket);
				TripBUS.updateTrip(trip.getId(), stationID, price);
				OnewayTicketDAO.updateStatus(owTicket.getId(), 2);
				OnewayTicketDAO.updatePrice(owTicket.getId(), price);
			} else {
				System.out.println("Invalid Station");
				OnewayTicketBUS.displayInfo(owTicket);
			}
		} else if( owTicket.getStatus() == 0 ) {
			System.out.println("Oneway Ticket is New");
			OnewayTicketBUS.displayInfo(owTicket);
		}else if( owTicket.getStatus() == 2 ){
			System.out.println("Oneway Ticket is Destroyed");
			OnewayTicketBUS.displayInfo(owTicket);
			
		}
	}
}
