package businessLayer.ticket;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import dataLayer.TwentyfourHoursTicketDAO;
import dataTransferObject.TwentyfourHoursTicket;
import hust.soict.se.customexception.InvalidIDException;
import presentationLayer.GateInterface;
import presentationLayer.TicketRecognizerInterface;


/**
 * @author Nguyen Dinh Minh
 *
 */
public class TwentyfourHoursTicketBUS extends TicketBUS implements TwentyfourHoursTicketInterface {
	
	/**
	 *@param id
	 *@return
	 *@throws ClassNotFoundException, SQLException
	 */
	public TwentyfourHoursTicket getTicketById(String id) throws ClassNotFoundException, SQLException {
		return TwentyfourHoursTicketDAO.findbyId(id);
	}
	
	/**
	 * @param code
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static TwentyfourHoursTicket getTicketByCode(String code) throws ClassNotFoundException, SQLException {
		return TwentyfourHoursTicketDAO.getByCode(code);
	}
	
	/**
	 * @param tfhTicket
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static int checkTFActive(TwentyfourHoursTicket tfhTicket) 
			throws ClassNotFoundException,SQLException {
		String status = tfhTicket.getStatus();
		if (status.equals("activated")) {
			return 1;
		} else if (status.equals("unactivated")) {
			return 0;
		} else
			return -1;
	}
	
	/**
	 * @param tfhTicket
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws NullPointerException
	 */
	public static Timestamp getTFExpireTime(TwentyfourHoursTicket tfhTicket)
			throws ClassNotFoundException,SQLException,NullPointerException {
		Calendar cal = Calendar.getInstance();
		Timestamp expireTime;
		cal.setTimeInMillis(tfhTicket.getStartTime().getTime());
		cal.add(Calendar.DATE, 1);
		expireTime = new Timestamp(cal.getTime().getTime());
		
		return expireTime;
	}
	
	/**
	 * @param tfhTicket
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws NullPointerException
	 */
	public static int checkValidTFTicket(TwentyfourHoursTicket tfhTicket)
			throws ClassNotFoundException, SQLException,NullPointerException {
		int status = checkTFActive(tfhTicket);
		
		if (status == 1) {
			//if ticket activated, check valid
			Timestamp expireTime = getTFExpireTime(tfhTicket);
			Timestamp now = new Timestamp(System.currentTimeMillis());
			if (expireTime.after(now)) {
				// valid-time ticket
				return 1;
			} else {
				//invalid-time ticket
				return -1;
			}
		} else if (status == 0) {
			// if ticket unactivated, return 0
			return 0;
		} else {
			// invalid status
			return -2;
		}
	}
	
	/**
	 * @param tfhTicket
	 * @throws ClassNotFoundException
	 * @throws NullPointerException
	 * @throws SQLException
	 */
	public static void displayTfhTicketInfo(TwentyfourHoursTicket tfhTicket)
			throws ClassNotFoundException, NullPointerException, SQLException {
		Timestamp expireTime = getTFExpireTime(tfhTicket);
		
		String pattern = "HH:mm - dd 'of' MMM, yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String etime = simpleDateFormat.format(expireTime);
		String stime = simpleDateFormat.format(tfhTicket.getStartTime());
		
		System.out.println("Opening gate...");
		System.out.println("\t  24-HOUR TICKET");
		System.out.println("\tID: "+tfhTicket.getId());
		System.out.println("\t  Price: "+ tfhTicket.getPrice()+" eur");
		System.out.println("\tActivated seen: "+stime);
		System.out.println("\tValid until: "+etime);
	}
	
	/**
	 * @param tfhTicket
	 * @throws ClassNotFoundException
	 * @throws NullPointerException
	 * @throws SQLException
	 */
	public static void displayInvalidTfhTicketInfo(TwentyfourHoursTicket tfhTicket) 
			throws ClassNotFoundException, NullPointerException, SQLException {
		Timestamp expireTime = getTFExpireTime(tfhTicket);
		Timestamp now = new Timestamp(System.currentTimeMillis());

		String pattern = "HH:mm - dd 'of' MMM, yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String etime = simpleDateFormat.format(expireTime);
		String nowtime = simpleDateFormat.format(now);
		
		System.out.println("Invalid 24h Ticket");
		System.out.println("ID: "+tfhTicket.getId());
		System.out.println("\tValid until: "+etime);
		System.out.println("Error: ticket expried, tried to enter at "+nowtime);
		
	}
	
	/**
	 * @param tfhTicket
	 * @throws ClassNotFoundException
	 * @throws NullPointerException
	 * @throws SQLException
	 */
	public static void displayExpiredMessage(TwentyfourHoursTicket tfhTicket) 
			throws ClassNotFoundException, NullPointerException, SQLException {
		Timestamp expireTime = getTFExpireTime(tfhTicket);
		Timestamp now = new Timestamp(System.currentTimeMillis());

		String pattern = "HH:mm - dd 'of' MMM, yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String etime = simpleDateFormat.format(expireTime);
		String nowtime = simpleDateFormat.format(now);
		
		System.out.println("24h Ticket expire");
		System.out.println("ID: "+tfhTicket.getId());
		System.out.println("\tTicket expried since: "+etime);
	}
	
	
	/**
	 * @param pseudoBarCode
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void processTfhTicketExit(String pseudoBarCode)
			throws InvalidIDException, ClassNotFoundException, SQLException {
			TwentyfourHoursTicket tfhTicket = new TwentyfourHoursTicket();
			String ticketCode=TicketRecognizerInterface.process(pseudoBarCode);
			tfhTicket = getTicketByCode(ticketCode);
			int tfhValid = checkValidTFTicket(tfhTicket);
			
			if (tfhTicket.getId() == null) {
				System.out.println("Twentyfour Hours Ticket does not exist");
				GateInterface.close();
			} 
			// if ticket unactivated ( tfhValid == 0)
			else if (tfhValid == 0) {
				Timestamp now = new Timestamp(System.currentTimeMillis());  
				tfhTicket.setStartTime(now);
				String pattern = "yyyy-MM-dd HH:mm:ss";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
				String time = simpleDateFormat.format(tfhTicket.getStartTime());
				TwentyfourHoursTicketDAO.setTicketStartTime(tfhTicket.getId(), time);
				displayTfhTicketInfo(tfhTicket);
			}
			// if ticket time valid
			else if (tfhValid == 1) {
				//open Gate
				GateInterface.open();
				//print ticket Info
				displayTfhTicketInfo(tfhTicket);
			} else if (tfhValid == -1) {
				//close Gate
				GateInterface.open();
				//print expire error
				displayExpiredMessage(tfhTicket);
//				System.out.println("\tInvalid! \n24-HOUR TICKET has expired.");
			} else if (tfhValid == -2) {
				// close Gate
				GateInterface.close();
				//print ticket status error
				displayTfhTicketInfo(tfhTicket);
				System.out.println("\tInvalid! \n24-HOUR TICKET has status-error.");
			}
		}
	
	/**
	 * @param pseudoBarCode
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void processTfhTicket(String pseudoBarCode)
		throws InvalidIDException, ClassNotFoundException, SQLException {
		TwentyfourHoursTicket tfhTicket = new TwentyfourHoursTicket();
		String ticketCode=TicketRecognizerInterface.process(pseudoBarCode);
		tfhTicket = getTicketByCode(ticketCode);
		int tfhValid = checkValidTFTicket(tfhTicket);
		
		if (tfhTicket.getId() == null) {
			System.out.println("Twentyfour Hours Ticket does not exist");
			GateInterface.close();
		} 
		// if ticket unactivated ( tfhValid == 0)
		else if (tfhValid == 0) {
			Timestamp now = new Timestamp(System.currentTimeMillis());  
			tfhTicket.setStartTime(now);
			String pattern = "yyyy-MM-dd HH:mm:ss";
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
			String time = simpleDateFormat.format(tfhTicket.getStartTime());
			TwentyfourHoursTicketDAO.setTicketStartTime(tfhTicket.getId(), time);
			displayTfhTicketInfo(tfhTicket);
		}
		// if ticket time valid
		else if (tfhValid == 1) {
			//open Gate
			GateInterface.open();
			//print ticket Info
			displayTfhTicketInfo(tfhTicket);
		} else if (tfhValid == -1) {
			//close Gate
			GateInterface.close();
			//print expire error
			displayInvalidTfhTicketInfo(tfhTicket);
//			System.out.println("\tInvalid! \n24-HOUR TICKET has expired.");
		} else if (tfhValid == -2) {
			// close Gate
			GateInterface.close();
			//print ticket status error
			displayTfhTicketInfo(tfhTicket);
			System.out.println("\tInvalid! \n24-HOUR TICKET has status-error.");
		}
	}
}