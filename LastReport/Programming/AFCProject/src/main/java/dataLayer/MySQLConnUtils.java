package dataLayer;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author Do Thuy Nga
 *
 */
public class MySQLConnUtils {
	// Kết nối vào MySQL.
		 /**
		 * @return
		 * @throws SQLException
		 * @throws ClassNotFoundException
		 */
		public static Connection getMySQLConnection() throws SQLException,ClassNotFoundException {
		     // tự config theo tên
			 String hostName = "localhost";
		     String dbName = "afc_database";
		     String userName = "root";
		     String password = "12345";
//		     String password = "1";
		 
		     return getMySQLConnection(hostName, dbName, userName, password);
		 }
		 
		 /**
		 * @param hostName
		 * @param dbName
		 * @param userName
		 * @param password
		 * @return
		 * @throws SQLException
		 * @throws ClassNotFoundException
		 */
		public static Connection getMySQLConnection(String hostName, String dbName,
		         String userName, String password) throws SQLException,
		         ClassNotFoundException {
		     Class.forName("com.mysql.cj.jdbc.Driver");

		     String connectionURL = "jdbc:mysql://" + hostName + ":3306/" + dbName + "?useSSL=false";
		 
		     Connection conn = DriverManager.getConnection(connectionURL, userName,
		             password);
		     return conn;
		 }
}
