package dataLayer;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import dataTransferObject.TwentyfourHoursTicket;

/**
 * @author Nguyen Dinh Minh
 *
 */
public class TwentyfourHoursTicketDAO {
	/**
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static ArrayList<TwentyfourHoursTicket> tfHoursTicketAll() throws ClassNotFoundException, SQLException{
		ArrayList<TwentyfourHoursTicket> arr = new ArrayList<TwentyfourHoursTicket>();
        String sql = "select * from tfhoursticket";
        
        Connection connection = ConnectionUtils.getMyConnection();

        Statement statement = connection.createStatement();
      
        ResultSet rs = statement.executeQuery(sql);
        
        while (rs.next()) {
        	TwentyfourHoursTicket tfhours = new TwentyfourHoursTicket();
        	tfhours.setId(rs.getString("id"));
        	tfhours.setStatus(rs.getString("status"));
        	tfhours.setPrice(rs.getDouble("price"));
        	tfhours.setStartTime(rs.getTimestamp("startTime"));
            arr.add(tfhours);
      
        }
        
        connection.close();
        return arr;
	}
	
	/**
	 * @param idTF
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static TwentyfourHoursTicket findbyId(String idTF) throws ClassNotFoundException, SQLException {
		TwentyfourHoursTicket tfhTicket = new TwentyfourHoursTicket();
		String sql = "SELECT * from tfhoursticket WHERE Id="+"\""+idTF +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
			tfhTicket.setId(rs.getString("id"));
			tfhTicket.setStatus(rs.getString("status"));
			tfhTicket.setPrice(rs.getDouble("price"));
			tfhTicket.setStartTime(rs.getTimestamp("startTime"));
			tfhTicket.setCode(rs.getString("code"));
        }
		connection.close();
		return tfhTicket;
	}

	/**
	 * @param code
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static TwentyfourHoursTicket getByCode(String code) throws ClassNotFoundException, SQLException {
		TwentyfourHoursTicket tfhTicket = new TwentyfourHoursTicket();
		String sql = "SELECT * from tfhoursticket WHERE Code="+"\""+code +"\"";
		Connection connection = ConnectionUtils.getMyConnection();
		Statement statement = connection.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		
		while (rs.next()) {
			tfhTicket.setId(rs.getString("id"));
			tfhTicket.setStatus(rs.getString("status"));
			tfhTicket.setPrice(rs.getDouble("price"));
			tfhTicket.setStartTime(rs.getTimestamp("startTime"));
			tfhTicket.setCode(rs.getString("code"));
			}
		connection.close();
		return tfhTicket;
	}
	
	/**
	 * @param id
	 * @param startTime
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static boolean setTicketStartTime(String id, String startTime) 
			throws ClassNotFoundException, SQLException {
		String active = "activated";
//		System.out.println("/t sql.startTime: "+startTime);
		String query1 = "UPDATE tfhoursticket SET startTime = ? WHERE id = ?";
		String query2 = "UPDATE tfhoursticket SET status = ? WHERE id = ?";
		Connection connection = ConnectionUtils.getMyConnection();

	    PreparedStatement pstmt1 = connection.prepareStatement(query1);
	    pstmt1.setString(2, id);
	    pstmt1.setString(1, startTime);
	    PreparedStatement pstmt2 = connection.prepareStatement(query2);
	    pstmt2.setString(2, id);
	    pstmt2.setString(1, active);
	    
	    pstmt1.execute();
	    boolean check = pstmt2.execute();
		
//		boolean check = statement.execute(query);
		connection.close();
		return check;
	}
}
