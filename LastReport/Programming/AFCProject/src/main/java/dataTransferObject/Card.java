package dataTransferObject;

/**
 * @author Vu Duc Nguyen
 * @email: ducnguyen272727@gmail.com
 *
 */
public class Card {
	private String id;
	private double balance;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
}
