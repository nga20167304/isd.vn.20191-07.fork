package dataTransferObject;

/**
 * @author Vu Duc Nguyen
 * @email: ducnguyen272727@gmail.com
 *
 */
public class PrepaidCard extends Card{
	private String code;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
}
