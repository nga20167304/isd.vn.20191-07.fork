package presentationLayer;

import java.io.IOException;
import java.sql.SQLException;

import hust.soict.se.customexception.InvalidIDException;

public class Main {

	public static void main(String[] args) 
			throws ClassNotFoundException, InvalidIDException, SQLException, IOException {
		GUI.start();
	}

}