package junitTest.businessLayer.card;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import businessLayer.card.PrepaidCardBUS;

public class PrepaidCardBUSTest {
	
	private PrepaidCardBUS prepaidCardBUSTest;
	@BeforeEach
	void setUp() throws Exception {
		prepaidCardBUSTest = new PrepaidCardBUS();
	}

	@Test
	public void testGetCardBalance() {
		double input, result;
		input = prepaidCardBUSTest.getCardBalance("PC201911112999");
		result = 49.00000000000003;
		assertEquals(result, input);
		
		input = prepaidCardBUSTest.getCardBalance("7bf52afd1d2eb936");
		result = 42.6;
		assertNotEquals(result, input);
		
		input = prepaidCardBUSTest.getCardBalance("PC201911121234");
		result = 1.5;
		assertEquals(result, input);
		
		input = prepaidCardBUSTest.getCardBalance("PC201911112999");
		result = 49;
		assertNotEquals(result, input);
	}

	@Test
	public void testCheckBalanceFirst() {
		boolean input, result;
		input = prepaidCardBUSTest.checkBalanceFirst("PC201911112999");
		result = true;
		assertEquals(result, input);
		
		input = prepaidCardBUSTest.checkBalanceFirst("PC201911121234");
		result = false;
		assertEquals(result, input);
		
		input = prepaidCardBUSTest.checkBalanceFirst("PC201911121234");
		result = true;
		assertNotEquals(result, input);
		
		input = prepaidCardBUSTest.checkBalanceFirst("PC201911112999");
		result = false;
		assertNotEquals(result, input);
	}

	@Test
	public void testCheckBalanceWhenExit() {
		boolean input, result;
		input = prepaidCardBUSTest.checkBalanceWhenExit("PC201911112999");
		result = true;
		assertEquals(result, input);
		
		input = prepaidCardBUSTest.checkBalanceWhenExit("PC201911121234");
		result = false;
		assertNotEquals(result, input);
		
		input = prepaidCardBUSTest.checkBalanceWhenExit("PC201911121234");
		result = true;
		assertEquals(result, input);
		
		input = prepaidCardBUSTest.checkBalanceWhenExit("PC201911112999");
		result = false;
		assertNotEquals(result, input);
	}
}
