# ISD.VN.Group 07

Final Report assignment

# Phân công công việc
## Đỗ Thúy Nga
* Làm phần 2c - Chỉ rõ và phân tích các mức Coupling và Cohesion của bản thiết kế
* Làm phần 3 - Bản đặc tả chương trình (program spec chính là phần đặc tả cho mã nguồn, được sinh ra từ Java doc comment) và thiết kế kiểm thử đơn vị
* Code phần Ticket, One-way Ticket, Station, Connect to database
* Viết junitTest phần One-way Ticket

## Vũ Đức Nguyễn
* Làm phần 2d - Phân tích việc tuân thủ các design principles trong SOLID và áp dụng các mẫu thiết kế như thế nào
* Làm phần 3 - Bản đặc tả chương trình (program spec chính là phần đặc tả cho mã nguồn, được sinh ra từ Java doc comment) và thiết kế kiểm thử đơn vị
* Làm phần 5 - Video demo chương trình 
* Code phần Card, Prepaid-card, Trip, Check Valid Input
* Viết JunitTest phần Card, PrepaidCard, CheckInput
* Refactoring code

## Nguyễn Đình Minh
* Sửa lại SRS AFC-SRS-ISD.20191.07.docx của các tuần trước cả nhóm làm
* Trong AFC-SDD-ISD.20191.07.docx:
* Trong mục 1.Structure Design: phần 1.1 - 1.2 - 1.2.1
* Trong mục 2.Class Design: phần 2.1 - 2.2 - 2.3 - 2.4
* Trong mục 3.Data Modeling: phần 3.1 - 3.2 - 3.3
* Code phần Ticket, 24h Ticket
* Viết JunitTest phần 24h Ticket

## Nguyễn Trọng Nghĩa
* Làm phần 2c - Chỉ rõ và phân tích các mức Coupling và Cohesion của bản thiết kế, 2e
* Làm phần 2e - Phân tích các ưu điểm của những việc này đối với những yêu cầu mới cô đã cung cấp
* Code phần GUI

## Đỗ Quốc Nam
* Có tham gia làm 1 số tuần đầu.

# Đánh giá sự hoàn thành và đóng góp của thành viên trong nhóm
## Đỗ Thúy Nga
* Hoàn thành tốt nhiệm vụ được giao, đúng deadline.
* Nhiệt tình tham gia các cuộc thảo luận trong nhóm, phát triển nhóm.
* Cầu nối của các bạn.

## Vũ Đức Nguyễn
* Hoàn thành tốt nhiệm vụ được giao, đúng deadline.
* Nhiệt tình tham gia các cuộc thảo luận trong nhóm, phát triển nhóm.
* Đưa ra nhiều ý kiến, suy nghĩ.

## Nguyễn Đình Minh
* Hoàn thành tốt nhiệm vụ được giao, đúng deadline.
* Nhiệt tình tham gia các cuộc thảo luận trong nhóm, phát triển nhóm.
* Không nên "một mình một ý", nên bàn bạc với nhóm để có cách giải quyết tốt nhất.

## Nguyễn Trọng Nghĩa
* Hoàn thành tốt nhiệm vụ được giao, đúng deadline.
* Nhiệt tình tham gia các cuộc thảo luận trong nhóm, phát triển nhóm
* Hơi ít đưa ra ý kiến.

## Đỗ Quốc Nam
* ???
