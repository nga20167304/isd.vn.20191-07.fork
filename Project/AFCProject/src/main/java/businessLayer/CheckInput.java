package businessLayer;

/**
 * @author Vu Duc Nguyen
 * email: ducnguyen272727@gmail.com
 *
 */
public class CheckInput {
	/**
	 * @param inputMainScreen
	 * @return 
	 */
	public static int checkEnterExit(String inputMainScreen) {
		if (inputMainScreen.charAt(0) == '1') {
			return 1;
		} else if (inputMainScreen.charAt(0) == '2') {
			return 2;
		} else {
			return 0;
		}
	}
	
	// kiem tra ki tu '-'
	/**
	 * @param inputMainScreen
	 * @return
	 */
	public static boolean checkInput(String inputMainScreen) {
		if(inputMainScreen.charAt(1) == '-') {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param inputMainScreen
	 * @return
	 */
	public static boolean checkStation(String inputMainScreen) {
		if (inputMainScreen.charAt(2) >= 'a' && inputMainScreen.charAt(2) <= 'i') {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * @param pseudoBarCode
	 * @return
	 */
	public static int checkPseudoBarCode(String pseudoBarCode) {
		int checkNumber = 1;
		for(int i = 0; i < pseudoBarCode.length(); i++) {
			if (pseudoBarCode.charAt(i) >= '1' && pseudoBarCode.charAt(i) <= '9') {
				checkNumber = -1;
				break;
			}
		}
		if( pseudoBarCode.length() != 8 || checkNumber == -1) {
			return -1; // errors length or number
		} else {
			if( pseudoBarCode.equals(pseudoBarCode.toUpperCase()) ) {
				return 1; // 1 la prepaid card
			} else if ( pseudoBarCode.equals(pseudoBarCode.toLowerCase()) ) {
				return 2; // 0 la ticket
			} else {
				return 0; // errors
			}
		}
	}
}
