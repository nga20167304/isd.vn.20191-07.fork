package businessLayer.ticket;

import java.sql.SQLException;
import java.text.DecimalFormat;

import dataLayer.StationDAO;
import dataLayer.TicketDAO;
import dataTransferObject.OnewayTicket;
import dataTransferObject.Ticket;
import hust.soict.se.customexception.InvalidIDException;
import presentationLayer.TicketRecognizerInterface;

/**
 * @author Do Thuy Nga,Nguyen DInh Minh
 *
 */
public class TicketBUS {
	/**
	 * @param code
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static int getTicketType(String code) throws ClassNotFoundException, SQLException {
		Ticket ticket=TicketDAO.findbyCode(code);
		return ticket.getType();
	}
	
	/**
	 * @param pseudoBarCode
	 * @param stationID
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws NullPointerException
	 */
	public static void handleTicketWhenEnterStation(String pseudoBarCode,int stationID) 
			throws InvalidIDException, ClassNotFoundException, SQLException, NullPointerException {
		String ticketCode=TicketRecognizerInterface.process(pseudoBarCode);
		if(getTicketType(ticketCode)==1)
			OnewayTicketBUS.handleOnewayTicketWhenEnterStation(pseudoBarCode, stationID);
		else if(getTicketType(ticketCode)==2) {
			//handle 24h  ticket
			System.out.println("Enter with 24 HOUR Ticket");
			TwentyfourHoursTicketBUS.processTfhTicket(pseudoBarCode);
		}
		else System.out.println("Invalid Ticket code");
	}
	
	/**
	 * @param pseudoBarCode
	 * @param stationID
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void handleTicketWhenExitStation(String pseudoBarCode,int stationID) throws InvalidIDException, ClassNotFoundException, SQLException {
		String ticketCode=TicketRecognizerInterface.process(pseudoBarCode);
		if(getTicketType(ticketCode)==1)
			OnewayTicketBUS.handleOnewayTicketWhenExitStation(pseudoBarCode, stationID);
		else if(getTicketType(ticketCode)==2) {
			//handle 24h  ticket
			System.out.println("Exit with 24 HOUR Ticket");
			TwentyfourHoursTicketBUS.processTfhTicketExit(pseudoBarCode);
		}
		else System.out.println("Invalid Ticket code");
	}
	
	/**
	 * @param enterStationID
	 * @param exitStationID
	 * @param ticket
	 * @return
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static double caculate(int enterStationID,int exitStationID,Ticket ticket)
			throws ClassNotFoundException, SQLException {
		Double enter=StationDAO.getDistanceById(enterStationID);
		Double exit=StationDAO.getDistanceById(exitStationID);
		Double distance=exit-enter;
		Double price=ticket.getPrice();
		Double fare=(double) 0;
		if(distance<=5) 
			fare=1.9;
		else {
			fare=1.9;
			Double distance1=distance-5;
			while(distance1-2>=0) {
				fare+=0.4;
				distance1=distance1-2;
			}
		}
		ticket.setPrice(price-fare);
		return ticket.getPrice();
	}
	
	/**
	 * @param ticket
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public static void displayInfo(Ticket ticket) throws ClassNotFoundException, SQLException {
		DecimalFormat df = new DecimalFormat("#.00");
		System.out.println("Type: ONE-WAY TICKET\t"+"ID: "+ticket.getId());
		System.out.println("Balance: "+ df.format(ticket.getPrice())+" euros");
		
	}
}