package dataTransferObject;

import java.sql.Date;

/**
 * @author Vu Duc Nguyen
 * email: ducnguyen272727@gmail.com
 */
public class Trip {
	private int id;
	private String ticketCardID;
	private int departureStationID;
	private Date departureTime;
	private int arrivalStationID;
	private Date arrivalTime;
	private int status;
	private double price;
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTicketCardID() {
		return ticketCardID;
	}
	public void setTicketCardID(String ticketCardID) {
		this.ticketCardID = ticketCardID;
	}
	public int getDepartureStationID() {
		return departureStationID;
	}
	public void setDepartureStationID(int departureStationID) {
		this.departureStationID = departureStationID;
	}
	public Date getDepartureTime() {
		return departureTime;
	}
	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}
	public int getArrivalStationID() {
		return arrivalStationID;
	}
	public void setArrivalStationID(int arrivalStationID) {
		this.arrivalStationID = arrivalStationID;
	}
	public Date getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(Date arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
}
