package presentationLayer;

import businessLayer.*;
import businessLayer.card.PrepaidCardBUS;
import businessLayer.ticket.TicketBUS;
import businessLayer.ticket.TwentyfourHoursTicketBUS;
import dataLayer.OnewayTicketDAO;
import dataLayer.PrepaidCardDAO;
import dataLayer.StationDAO;
import dataLayer.TwentyfourHoursTicketDAO;
import dataTransferObject.OnewayTicket;
import dataTransferObject.PrepaidCard;
import dataTransferObject.TwentyfourHoursTicket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.Scanner;
//import java.util.Set;
import java.util.*;

import hust.soict.se.customexception.InvalidIDException;


/**
 * @author Nguyen Trong Nghia
 *
 */
public class GUI {
	private static Set<Map.Entry<String, String>> set;

	/**
	 * @return
	 * @throws InvalidIDException
	 */
	public static String mainScreen() throws InvalidIDException {
		System.out.println("These are stations in the line M14 of Paris");
		System.out.print("a. Saint-Lazare\n" +
				"b. Madeleine\n" + 
				"c. Pyramides\n" + 
				"d. Chatelet\n" + 
				"e. Gare de Lyon\n" + 
				"f. Bercy\n" + 
				"g. Cour Saint-Emilion\n" + 
				"h. Bibliotheque Francois Mitterrand\n" + 
				"i. Olympiades\n");
		System.out.println("Available actions: 1-enter station, 2-exit station");
		System.out.println("You can provide a combination of number (1 or 2) \n"
				+ "and a letter from (a to i) to enter or exit a station (using hyphen in between).\n"
				+ "For instance, the combination 2-d will bring you to exit the station Chatelet.");
		System.out.print("Your input: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		return input;
	}
	
	public static void clrscr() {
		// todo
	}
	
	public static void pressAnyKeyToContinue()
	 { 
		System.out.println("Press Enter key to continue...");
        try {
            System.in.read();
        } catch(Exception e) {
        	
        }
	 }
	
	/**
	 * @return
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InvalidIDException 
	 */
	public static String choosingATicketCard() throws ClassNotFoundException, SQLException, InvalidIDException {
		clrscr();
		System.out.println("Choosing a ticket/card:");
		System.out.println("These are exsting tickets/cards:");
		readFile();
		System.out.print("Please provide the ticket code you want to enter/exit: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		return input;
	}
	
	/**
	 * @throws InvalidIDException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 * @throws IOException 
	 */
	public static void start() throws InvalidIDException, ClassNotFoundException, SQLException, IOException {
		
		PrepaidCardBUS prepaidCardBUS = PrepaidCardBUS.getInstance();
		int stationID=0;
		String  pseudoBarCode = null, ticketCode = null;
		while (true) {
			String inputMainScreen = mainScreen();
			
			// xu ly inputMainScreen
			if(inputMainScreen.equals("q")) {
				System.out.println("Good bye");
				return;
			}
			
			if(inputMainScreen.length() != 3) {
				System.out.println("Systax Error");
				continue;
			}
			
			if (CheckInput.checkEnterExit(inputMainScreen) == 0 || 
				CheckInput.checkInput(inputMainScreen) == false ||
				CheckInput.checkStation(inputMainScreen) == false) {
				System.out.println("Systax Error");
				continue;
//				return;
			}
			stationID = inputMainScreen.charAt(2)-'a'+1;
			switch (CheckInput.checkEnterExit(inputMainScreen)) {
			case 1:
				pseudoBarCode = choosingATicketCard();
				if( CheckInput.checkPseudoBarCode(pseudoBarCode) == -1) {
					System.out.println("Error lengths or number");
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 0) {
					System.out.println("Error pseudoBarCode");
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 1) {
					// handle Card
					prepaidCardBUS.handleCardWhenEnterStation(pseudoBarCode, stationID);
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 2) {
					// handle Ticket
					//System.out.println("Ticket");
					TicketBUS.handleTicketWhenEnterStation(pseudoBarCode, stationID);
				}
				pressAnyKeyToContinue();
				break;
			case 2:
				pseudoBarCode = choosingATicketCard();
//				System.out.println(pseudoBarCode);
				if( CheckInput.checkPseudoBarCode(pseudoBarCode) == -1) {
					System.out.println("Error lengths or number");
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 0) {
					System.out.println("Error pseudoBarCode");
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 1) {
					// handle Card
					prepaidCardBUS.handleCardWhenExitStation(pseudoBarCode, stationID);
				} else if( CheckInput.checkPseudoBarCode(pseudoBarCode) == 2) {
					// handle Ticket
					TicketBUS.handleTicketWhenExitStation(pseudoBarCode, stationID);
				}
				pressAnyKeyToContinue();
				break;
			default:
				System.out.println("Systax Error");
				break;
			}
		}
		
	}
	
	/**
	 * @throws SQLException 
	 * @throws ClassNotFoundException 
	 * @throws InvalidIDException 
	 * 
	 */
	public static void readFile() throws ClassNotFoundException, SQLException, InvalidIDException {
		BufferedReader br = null;
		DecimalFormat df = new DecimalFormat("#.00");
        try {   
//            br = new BufferedReader(new FileReader("C:\\Users\\Admin\\Desktop\\isd.vn.20191.07.fork\\Project\\Database\\information.txt"));
        	br = new BufferedReader(new FileReader("/home/admina/Documents/itss/isd.vn.20191-07-fork/Project/Database/information.txt"));
            
            String textInALine="";
            
           HashMap<String, String> map = new HashMap<String, String>();
            while ((textInALine = br.readLine()) != null) {
            	String array1[] = textInALine.split(":");
//                System.out.println(textInALine);
                map.put(array1[0], array1[1]);
                //textInALine = br.readLine();
            }
//            set = map.entrySet();
            Set<Map.Entry<String, String>> set = map.entrySet();
            OnewayTicket owTicket;
            TwentyfourHoursTicket thTicket;
            PrepaidCard card;
            for(String id : map.keySet()) {
            	String type=map.get(id);
            	if(type.equals("OW")) {
            		String onewayCode = TicketRecognizerInterface.process(id);
            		owTicket=OnewayTicketDAO.getByCode(onewayCode);
            		String statuString = "";
            		if(owTicket.getStatus() == 0) {
            			statuString = "New";
            		} else if (owTicket.getStatus() == 1) {
            			statuString = "In Station";
            		} else if (owTicket.getStatus() == 2 ){
						statuString = "Destroyed";
					}
            		System.out.println(id+": One-way ticket between "+StationDAO.getStationNameById(owTicket.getEmbarkation())
            							+ " and " + StationDAO.getStationNameById(owTicket.getDisembarkation())
            							+ ": "+ statuString +" - "+ df.format(owTicket.getPrice())+" euros");
            	}
            	if(type.equals("TH")) {
            		String thCode = TicketRecognizerInterface.process(id);
            		thTicket=TwentyfourHoursTicketDAO.getByCode(thCode);
            		String statuString = "";
            		if( thTicket.getStatus().equals("unactivated")) {
            			statuString = "New";
            		} else if (thTicket.getStatus().equals("activated")) {
            			Object Timestamp = TwentyfourHoursTicketBUS.getTFExpireTime(thTicket);
            			statuString = "Valid until " + Timestamp;
            		}
            		System.out.println(id+": 24h tickets: " + statuString);
            	}
            	if(type.equals("PC")) {
            		String cardCode = CardScannerInterface.process(id);
            		card=PrepaidCardDAO.getInstance().getCardByCode(cardCode);
            		System.out.println(id + ": Prepaid card: " + df.format(card.getBalance())+" euros");
            	}
            }
         

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        
	}
	
	/**
	 * @param id
	 * @param name
	 * @throws IOException
	 */
	public static void writeFile(String id,String name) throws IOException {
//		FileWriter writer = new FileWriter("C:\\Users\\Admin\\Desktop\\isd.vn.20191.07.fork\\Project\\Database\\information.txt");
		FileWriter writer = new FileWriter("/home/admina/Documents/itss/isd.vn.20191-07-fork/Project/Database/information.txt");
        BufferedWriter buffer = new BufferedWriter(writer);
        buffer.write(id +":" +name);
        buffer.close();
	}
}
