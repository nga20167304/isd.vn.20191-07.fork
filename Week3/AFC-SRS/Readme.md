# Work assignment in AFC-SRS: #
* Leader: Nguyễn Đình Minh
## Đỗ Thúy Nga ##
  1. Đặc tả use case UC003 “Validate ticket (when exit the platform area)”
  2. Quy trình dùng vé (ticket) ra khỏi khu vực đợi tàu (exit the platform area)

## Nguyễn Đình Minh ##
  1. Đặc tả use case UC004 “Validate card (when exit the platform area)”
  2. Quy trình dùng thẻ (card) ra khỏi khu vực đợi tàu (exit the platform area)
  3. Dựa trên form SRS-UGMS-Sample-VN.doc tạo file AFC-SRS-Group07.docx

## Nguyễn Trọng Nghĩa ##
  1. Đặc tả use case UC001 “Validate ticket (when enter the platform area)”
  2. Quy trình dùng vé (ticket) vào khu vực đợi tàu (enter the platform area)

## Vũ Đức Nguyễn ##
  1. Đặc tả use case UC002 “Validate card (when enter the platform area)”
  2. Quy trình dùng thẻ (card) vào khu vực đợi tàu (enter the platform area)
