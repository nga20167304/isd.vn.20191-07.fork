# Work assignment in Architecture Design AFC

* Leader: Nguyễn Đình Minh

## Đỗ Thúy Nga

### 2 tasks

* Redesign the Use case Diagrams

* Do the sequence diagrams for: Enter the platform with one-way ticket

## Nguyễn Trọng Nghĩa

### Do the sequence diagrams for

* Exit the platform area with one-way ticket

* Enter the platform area with 24h ticket

## Vũ Đức Nguyễn

### Do the sequence diagrams for

* Exit the platform area with 24h ticket

* Enter the platform area with prepaid card

## Nguyễn Đình Minh

### 2 tasks

* Do the sequence diagrams for: Exit the platform with prepaid card

* Create team file, reformat member's file, summarize homework
