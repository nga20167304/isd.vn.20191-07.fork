Reviewer: Nguyễn Đình Minh
review for: Đỗ Thúy Nga
Content:
- Class Diagrams nên đổi tên SystemController thành cụ thể hơn
- Communication Diagrams:
  + Ở Ticket Recognizer chỉ đảm nhận việc lấy thông tin từ vé, không có check
  + 1.1.3.4.1 chưa có chiều mũi tên
  + Căn chỉnh lại cho hình đỡ bị rối
