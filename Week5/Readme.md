# ISD.VN.20191-07 - Homework week5

* All members's homework are in All Diagrams folder

# Work assignment in Architecture Design AFC

* Leader: Nguyễn Đình Minh

## Đỗ Thúy Nga

### Sửa lại sequence diagram

* Enter the platform with one-way ticket

### Vẽ Communication diagram

* Enter the platform with one-way ticket

### Analysis class diagram

* Enter the platform with one-way ticket

## Nguyễn Trọng Nghĩa

### Sửa lại sequence diagram

* Exit the platform area with one-way ticket

* Enter the platform area with 24h ticket

### Vẽ Communication diagram

* Exit the platform area with one-way ticket

* Enter the platform area with 24h ticket

### Analysis class diagram

* Exit the platform area with one-way ticket

* Enter the platform area with 24h ticket

## Vũ Đức Nguyễn

### Sửa lại sequence diagram

* Enter the platform area with prepaid card

### Vẽ Communication diagram

* Enter the platform area with prepaid card

### Analysis class diagram

* Enter the platform area with prepaid card

## Đỗ Quốc Nam

### Sửa lại sequence diagram

* Exit the platform area with 24h ticket

### Vẽ Communication diagram

* Exit the platform area with 24h ticket

### Analysis class diagram

* Exit the platform area with 24h ticket

## Nguyễn Đình Minh

### Sửa lại sequence diagram

* Exit the platform area with prepaid card

### Vẽ Communication diagram

* Exit the platform area with prepaid card

### Analysis class diagram

* Exit the platform area with prepaid card

### Combined class diagram cho toàn hệ thống
