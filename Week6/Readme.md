# ISD.VN.20191-07 - Homework Week 6

* All members's homework are in All Diagrams folder

# Work assignment in Architecture Design AFC

* Leader: Nguyễn Đình Minh

## Đỗ Thúy Nga

### Vẽ Specification Design Class Diagram

* Enter the platform with one-way ticket

## Nguyễn Trọng Nghĩa

### Vẽ Specification Design Class Diagram

* Exit the platform area with one-way ticket

* Enter the platform area with 24h ticket

## Vũ Đức Nguyễn

### Vẽ Specification Design Class Diagram

* Enter the platform area with prepaid card

## Đỗ Quốc Nam

### Vẽ Specification Design Class Diagram

* Exit the platform area with 24h ticket

## Nguyễn Đình Minh

### Vẽ Specification Design Class Diagram

* Exit the platform area with prepaid card

### Combined class diagram cho toàn hệ thống
