package beforeThreeLayer.application;

import java.util.Scanner;

public class Application {
	public static String mainScreen() {
		System.out.println("These are stations in the line M14 of Paris");
		System.out.print("a. Saint-Lazare\n " +
				"b. Madeleine\n" + 
				"c. Pyramides\n" + 
				"d. Chatelet\n" + 
				"e. Gare de Lyon\n" + 
				"f. Bercy\n" + 
				"g. Cour Saint-Emilion\n" + 
				"h. Bibliotheque Francois Mitterrand\n" + 
				"i. Olympiades\n");
		System.out.println("Available actions: 1-enter station, 2-exit station");
		System.out.print("Your input: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		return input;
	}
	
	public static int checkEnterExit(String inputMainScreen) {
		if (inputMainScreen.charAt(0) == '1') {
			return 1;
		} else if (inputMainScreen.charAt(0) == '2') {
			return 2;
		} else {
			return 0;
		}
	}
	
	// kiem tra ki tu '-'
	public static boolean checkInput(String inputMainScreen) {
		if(inputMainScreen.charAt(1) == '-') {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean checkStation(String inputMainScreen) {
		if (inputMainScreen.charAt(2) >= 'a' && inputMainScreen.charAt(2) <= 'i') {
			return true;
		} else {
			return false;
		}
	}
	
	public static String saveStation(char c) {
		String station;
		if ( c=='a') {
			return "Saint-Lazare";
		} else if (c == 'b') {
			return "Madeleine";
		} else if (c == 'c') {
			return "Pyramides";
		} else if (c == 'd') {
			return "Chatelet";
		} else if (c == 'e') {
			return "Gare de Lyon";
		} else if (c == 'f') {
			return "Bercy";
		} else if (c == 'g') {
			return "Cour Saint-Emilion";
		} else if (c == 'h') {
			return "Bibliotheque Francois Mitterrand";
		} else if (c == 'i') {
			return "Olympiades";
		}
		return "error";
	}
	
	public static String choosingATicketCard() {
		System.out.println("Choosing a ticket/card:");
		System.out.println("These are exsting tickets/cards:");
		// Todo
		System.out.print("Please provide the ticket code you want to enter/exit: ");
		Scanner scanner = new Scanner(System.in);
		String input = scanner.nextLine();
		return input;
	}
	public static void main(String[] args) {
		
		String choosingATicketCard, station;
		int checkEnterExit = 0;
		boolean checkInput, checkStation;
		String inputMainScreen = mainScreen();
		
		// xu ly inputMainScreen
		if(inputMainScreen.length() < 3) {
			System.out.println("Systax Error");
			return;
		}
		checkEnterExit = checkEnterExit(inputMainScreen);
		checkInput = checkInput(inputMainScreen);
		checkStation = checkStation(inputMainScreen);
		if (checkEnterExit == 0 || checkInput == false || checkStation == false) {
			System.out.println("Systax Error");
			return;
		}
		station = saveStation(inputMainScreen.charAt(2));
		switch (checkEnterExit) {
		case 1:
			System.out.println("Enter-Station: " + station);
			choosingATicketCard = choosingATicketCard();
			System.out.println(choosingATicketCard);
			// Todo
			break;
		case 2:
			System.out.println("Exit-Station: " + station);
			choosingATicketCard = choosingATicketCard();
			System.out.println(choosingATicketCard);
			// Todo
			break;
		default:
			System.out.println("Systax Error");
			break;
		}
	}
}
