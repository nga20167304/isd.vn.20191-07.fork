package beforeThreeLayer.testSDK.nguyentrongnghia;

import java.util.Scanner;
import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.scanner.CardScanner;

public class CardScannerTest {
	public static void main(String[] args) throws InvalidIDException{
		String pseudoBarCode = "ABCDWXYZ";
		System.out.print("Write 8 capitalise letters: ");
	    CardScanner cardScanner = CardScanner.getInstance();
	    try {
	    	String cardId = cardScanner.process(pseudoBarCode);
	    	System.out.print(cardId);
	    } catch(InvalidIDException errorMessage) {
	    	System.out.print(errorMessage);	 
	    }
	}
}
