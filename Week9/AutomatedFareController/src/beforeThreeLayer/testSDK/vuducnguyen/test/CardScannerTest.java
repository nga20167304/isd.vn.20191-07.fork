package beforeThreeLayer.testSDK.vuducnguyen.test;

import java.util.Scanner;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.scanner.CardScanner;
public class CardScannerTest {

	public static void main(String[] args) throws InvalidIDException{
		String pseudoBarCode = "ABCDEFVN";
		System.out.print("Nhap vao 1 string 8 ki tu viet hoa: ");
		Scanner scanner = new Scanner(System.in);
//		String pseudoBarCode = scanner.nextLine();
	    CardScanner cardScanner = CardScanner.getInstance();
	    try {
	    	String cardId = cardScanner.process(pseudoBarCode);
	    	System.out.print(cardId);
	    } catch(InvalidIDException e) {
	    	System.out.print(e);	 
	    }
	}
}
