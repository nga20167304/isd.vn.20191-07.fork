package dataLayer;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import dataTransferObject.OnewayTicket;

public class OnewayTicketDAO {
	public static ArrayList<OnewayTicket> onewayTicketAll() throws ClassNotFoundException, SQLException{
		ArrayList<OnewayTicket> arr = new ArrayList<OnewayTicket>();
        String sql = "select * from one-wayticket";
     // Láº¥y ra Ä‘á»‘i tÆ°á»£ng Connection káº¿t ná»‘i vÃ o DB.
        Connection connection = ConnectionUtils.getMyConnection();
   
        // Táº¡o Ä‘á»‘i tÆ°á»£ng Statement.
        Statement statement = connection.createStatement();
      
        ResultSet rs = statement.executeQuery(sql);
     // Duyá»‡t trÃªn káº¿t quáº£ tráº£ vá»�.
        while (rs.next()) {// Di chuyá»ƒn con trá»� xuá»‘ng báº£n ghi káº¿ tiáº¿p.
        	
        	OnewayTicket oneway = new OnewayTicket();
            oneway.setId(rs.getInt("Id"));
            oneway.setStatus(rs.getString("status"));
            oneway.setPrice(rs.getFloat("price"));
            oneway.setEmbarkation(rs.getString("embarkation"));
            oneway.setDisembarkation(rs.getString("disembarkation"));
            arr.add(oneway);
      
        }
        // Ä�Ã³ng káº¿t ná»‘i
        connection.close();
        return arr;
	}
	
	public static OnewayTicket findbyId(int idOneway) throws ClassNotFoundException, SQLException {
		ArrayList<OnewayTicket> arr=onewayTicketAll();
		for(OnewayTicket owt: arr) {
			if(owt.getId()==idOneway)
				return owt;
		}
		return null;
	}
}
