package presentationLayer;

import hust.soict.se.customexception.InvalidIDException;
import hust.soict.se.scanner.CardScanner;

public class CardScannerInterface {
	public static String process(String pseudoBarCode) throws InvalidIDException {
		CardScanner cardScanner = CardScanner.getInstance();
		return cardScanner.process(pseudoBarCode);
	}
}