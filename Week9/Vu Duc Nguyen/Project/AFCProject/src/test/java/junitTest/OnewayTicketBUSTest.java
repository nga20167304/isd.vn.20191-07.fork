package junitTest;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import businessLayer.OnewayTicketBUS;

public class OnewayTicketBUSTest {

	@Test
	public void testCaculateOnewayTicket() throws ClassNotFoundException, SQLException {
		float disembarkation=12,embarkation=(float) 5.5;
        float price=OnewayTicketBUS.caculateOnewayTicket(disembarkation,embarkation);
        assertNotEquals(price, 0);
	}

}
